package kunde;

public class GesellschaftMLiz extends AKunde {
	protected int vertrieb = 0;
		
	public GesellschaftMLiz(Double rabatt, int vertrieb) {
		super(rabatt);
		this.vertrieb = vertrieb;
	}

	public int getVertrieb() {
		return vertrieb;
	}

	public void setVertrieb(int vertrieb) {
		this.vertrieb = vertrieb;
	}

	public String toString() {
		return "GesellschaftMLiz: Vertrieb: " + getVertrieb() + " Kunden-Nr: "+ getNummer() + " Rabatt:" + getRabatt() + "% Anschrift:" + getAnschrift();		
	}
}

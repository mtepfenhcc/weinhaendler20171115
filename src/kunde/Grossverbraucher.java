package kunde;

public class Grossverbraucher extends AKunde {
	int status = 0;
	
	public Grossverbraucher(Double rabatt, int status) {
		super(rabatt);
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Grossverbraucher: Status: " + getStatus() + " Kunden-Nr: "+ getNummer() + " Rabatt:" + getRabatt() + "% Anschrift:" + getAnschrift();
	}

}

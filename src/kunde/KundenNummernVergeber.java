package kunde;

/**
 *  Hier werden Kundennummern vergeben.
 *  Das Objekt ist als Singleton realisiert, d.h. es soll
 *  nur einmal instanzierbar sein...
 * 
 * @author jlessmann
 *
 */

public class KundenNummernVergeber {
	private static KundenNummernVergeber uniqueInstance;

	private KundenNummernVergeber() {
	}
	public static KundenNummernVergeber getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new KundenNummernVergeber ();
		}
		return uniqueInstance;
	}
	
	public String getKundenNummer() {
		String kn = this.genKundenNummer();
		return kn;
	}
	
	private String genKundenNummer() {
		String kn = "";
		String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String num = "01234567890";
		kn += genRandomString(3, alpha.toLowerCase());
		kn += genRandomString(3, num);
		kn += genRandomString(3, alpha);
		return kn;
	}

	private String genRandomString(int charCnt, String chars) {
		String randStr = "";
		int j;
		for (int i = 0; i < charCnt; i++) {
			j = (int) (Math.random() * chars.length());
			randStr += chars.substring(j, j + 1);
		}
		return randStr;
	}
}

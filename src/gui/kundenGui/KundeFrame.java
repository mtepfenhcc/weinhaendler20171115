package gui.kundenGui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextArea;

import gui.bestellungGui.DetailPanel;

public class KundeFrame extends JFrame{
	private PanelKundeAnlegen panelKundeAnlegen;
	
	
	public KundeFrame(String title) {
		super(title);
		
		panelKundeAnlegen = new PanelKundeAnlegen();
		Container c = getContentPane();
		c.add(panelKundeAnlegen);
	}
}

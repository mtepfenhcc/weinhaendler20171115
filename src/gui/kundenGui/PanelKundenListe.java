package gui.kundenGui;

import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SingleSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import kunde.AKunde;
import kunde.Endverbraucher;
import kunde.GesellschaftMLiz;
import kunde.Grossverbraucher;
import kundenVerwaltung.Kundenverwaltung;
import javax.swing.ListSelectionModel;

public class PanelKundenListe extends JPanel {

	JList<AKunde> list;
	private DefaultListModel<AKunde> defaultListModel;

	class ListAktionen implements ListSelectionListener {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (!e.getValueIsAdjusting()) {
				int index = list.getSelectedIndex();
				if(index == -1) {
					PanelKundenArt.rdbtnEndverbraucher.setEnabled(false);
					PanelKundenArt.rdbtnGrossverbraucher.setEnabled(false);
					PanelKundenArt.rdbtnGesellschaftMLiz.setEnabled(false);
				}
				AKunde kunde = (AKunde) list.getSelectedValue();
				if (kunde instanceof Endverbraucher) {
					PanelKundenArt.rdbtnEndverbraucher.setSelected(true);
				} else if (kunde instanceof Grossverbraucher) {
					PanelKundenArt.rdbtnGrossverbraucher.setSelected(true);
				} else if (kunde instanceof GesellschaftMLiz) {
					PanelKundenArt.rdbtnGesellschaftMLiz.setSelected(true);
				}

			}

		}

	}

	public PanelKundenListe() {

		list = new JList<AKunde>();
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setModel(new AbstractListModel<AKunde>() {

			List<AKunde> values = Kundenverwaltung.getKunden();

			public int getSize() {
				return values.size();
			}

			public AKunde getElementAt(int index) {
				return values.get(index);
			}
		});
		list.addListSelectionListener(new ListAktionen());
		add(list);
		
	}

}

package model;

public class WeinSorte {
	private String weinsorte;
	private double preis;

	// Constructors
	public WeinSorte(String weinsorte, double preis) {
		this.weinsorte = weinsorte;
		this.preis = preis;
	}

	// Getters und Setters
	public String getWeinsorte() {
		return weinsorte;
	}

	public void setWeinsorte(String weinsorte) {
		this.weinsorte = weinsorte;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	// toString
	@Override
	public String toString() {
		return "Weinsorte: " + weinsorte + "\nPreis: " + preis;
	}

}
